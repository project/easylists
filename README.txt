Drupal easylists.module README.txt
==============================================================================

The Drupal easylists.module adds a filter to Drupal. It allow you to
create lists in a very intuitive way. For example:

- Part 1
- Part 2
-- Part 2 - Section a
-- Part 2 - Section b
- Part 3

... becomes (in HTML):

<ul>
  <li>Part 1</li>
  <li>Part 2
    <ul>
      <li>Part 2 - Section a</li>
      <li>Part 2 - Section b</li>
    </ul>
  </li>
  <li>Part3</li>
</ul>

Installation
------------------------------------------------------------------------------
 
  - Download the EasyList module from http://drupal.org/project/easylists

  - Create an easylists/ subdirectory in your modules directory and copy the
    files into it.

  - Enable the module as usual from Drupal's admin pages 
    (Administer » Modules)

  - Activate EasyLists in an input filter.
 
Credits / Contacts
------------------------------------------------------------------------------

  - The original author of this module is Frederic Jacquot (aka deelight), who
    can be reached at deelight[at]logeek.com. 

  - Most of the parsing code comes from MediaWiki 1.10.0. Thanks to its
    authors for releasing it under GPL.
