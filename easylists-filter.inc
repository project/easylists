<?php

// Credits: most of the parsing code is taken from MediaWiki 1.10.0
// (licenced under GPL2)

function _easylists_filter_process(&$body, $format = -1)
{
  $listprocessor = new listprocessor;
  $body = $listprocessor->process($body, variable_get('easylists_character_' . $format, '-'));
  return $body;
}

class listprocessor
{
  function process($body, $token)
  {
    $textLines = explode( "\n", $body );
    $lastPrefix = $output = '';
    $inBlockElem = false;
    $prefixLength = 0;
    $paragraphStack = false;
    foreach ( $textLines as $oLine )
    {
      // lists
      $oLine = trim($oLine);
      $lastPrefixLength = strlen( $lastPrefix );
      $prefixLength = strspn( $oLine, $token );
      $pref = substr( $oLine, 0, $prefixLength );
      $t = substr( $oLine, $prefixLength );
 
      if( $prefixLength && 0 == strcmp( $lastPrefix, $pref ) )
      {
        # Same as the last item, so no need to deal with nesting or opening stuff
        $output .= $this->nextItem( $token, substr( $pref, -1 ) );
        $paragraphStack = false;
      }
      elseif( $prefixLength || $lastPrefixLength )
      {
        # Either open or close a level...
        $commonPrefixLength = $this->getCommon( $pref, $lastPrefix );
        $paragraphStack = false;
        while( $commonPrefixLength < $lastPrefixLength )
        {
          $output .= $this->closeList( $token, $lastPrefix{$lastPrefixLength-1} );
          --$lastPrefixLength;
        }
        if ( $prefixLength <= $commonPrefixLength && $commonPrefixLength > 0 ) {
          $output .= $this->nextItem( $token, $pref{$commonPrefixLength-1} );
        }
        while ( $prefixLength > $commonPrefixLength ) {
          $char = substr( $pref, $commonPrefixLength, 1 );
          $output .= $this->openList( $token, $char, $prefixLength > 1 );
          ++$commonPrefixLength;
        }
        $lastPrefix = $pref;
      }
    
      // somewhere above we forget to get out of pre block (bug 785)
      if ($paragraphStack === false) {
        $output .= $t."\n";
      }
    }  
    while ( $prefixLength ) {
      $output .= $this->closeList( $token, $pref{$prefixLength-1} );
      --$prefixLength;
    }
    if ( '' != $this->mLastSection ) {
      $output .= '</' . $this->mLastSection . '>';
      $this->mLastSection = '';
    }

    return $output;
  }

  function getCommon( $st1, $st2 )
  {
    $fl = strlen( $st1 );
    $shorter = strlen( $st2 );
    if ( $fl < $shorter ) { $shorter = $fl; }

    for ( $i = 0; $i < $shorter; ++$i ) {
      if ( $st1{$i} != $st2{$i} ) { break; }
     }
    return $i;
  }

  function openList( $token, $char, $sublist=false )
  {
    $result = $this->closeParagraph();
    if ($sublist)
      $class = "easylist sublist";
    else
      $class = "easylist";

    if ( $token == $char ) { $result .= '<ul class="'.$class.'"><li>'; }
    else { $result = '<!-- ERR 1 -->'; }
    return $result;
  }

  function closeParagraph() {
    $result = '';
    if ( '' != $this->mLastSection ) {
      $result = '</' . $this->mLastSection  . ">\n";
    }
    $this->mInPre = false;
    $this->mLastSection = '';
    return $result;
  }

  function nextItem( $token, $char ) {
    if ( $token == $char ) { return '</li><li>'; }
    return '<!-- ERR 2 -->';
  }

  function closeList( $token, $char ) {
    if ( $token == $char ) { $text = '</li></ul>'; }
    else {  return '<!-- ERR 3 -->'; }
    return $text."\n";
  }
}
